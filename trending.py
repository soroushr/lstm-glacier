import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os, sys
from sklearn.preprocessing import RobustScaler, MinMaxScaler
from sklearn.metrics import mean_squared_error
import pandas as pd
from datetime import datetime
from util import preprocess_data, create_dataset
from fbprophet import Prophet

plt.rc('font', family='serif')

###########################
####### DEFINE PATHS
###########################
DATA_DIR = "./data/"

FIG_DIR = "./figs-corr-matrix/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

###########################
####### DATA PREP
###########################
def _get_rink_data(TRIM=False, RESAMPLE_FQ = 'W'):
    VEL_NAME = "17_terminus_velocity.xlsx"
    xls = pd.ExcelFile(DATA_DIR + VEL_NAME)
    vel = pd.read_excel(xls, "date_vel")
    term = pd.read_excel(xls, "date_terminus")

    # resample data to have uniform temporal frequency
    vel_resampled = preprocess_data(vel, var_column='vel', date_column='ds', resampling=RESAMPLE_FQ)
    term_resampled = preprocess_data(term, var_column='term', date_column='ds', resampling=RESAMPLE_FQ)
    merged_data = pd.merge_asof(vel_resampled, term_resampled, on='date')

    if TRIM:
        merged_data = merged_data[merged_data.date < datetime(2015,10,15)]
    
    return merged_data


def _get_helheim_data(TRIM=False, RESAMPLE_FQ = 'W'):
    VEL_NAME = "hlm_velocity_terminus.xlsx"
    xls = pd.ExcelFile(DATA_DIR + VEL_NAME)
    vel = pd.read_excel(xls, "velocity")
    term = pd.read_excel(xls, "terminus")

    # resample data to have uniform temporal frequency
    vel_resampled = preprocess_data(vel, var_column='vel', date_column='ds', resampling=RESAMPLE_FQ)
    term_resampled = preprocess_data(term, var_column='term', date_column='ds', resampling=RESAMPLE_FQ)
    merged_data = pd.merge_asof(vel_resampled, term_resampled, on='date')
    #merged_data = merged_data.dropna(axis=0)

    if TRIM:
        merged_data = merged_data[merged_data.date < datetime(2015,10,15)]

    return merged_data


def _separate_vars(data):
    data.index = pd.to_datetime(data.date)
    
    data_vel = data.drop(['term'], axis=1)
    data_term = data.drop(['vel'], axis=1)
    
    columns = ['ds', 'y']
    
    data_vel.columns = columns
    data_term.columns = columns    
    
    data_vel = data_vel.dropna()
    data_term = data_term.dropna()
    
    return data_vel, data_term


def fit_prophet(data):
    # returns a tuple with two components
    # first is date, second is trend values

    model=Prophet(interval_width=0.95) # by default is 80%
    model.fit(data)
    # forecast set to 0
    future = model.make_future_dataframe(periods=0)
    forecast = model.predict(future)
    forecast.index = pd.to_datetime(forecast.ds)

    # extract long term trend
    lines = model.plot_components(forecast)

    _trend = lines.get_axes()[0]
    trend = _trend.lines[0].get_data()

    return trend


def scale_stuff(data):
    scaler = MinMaxScaler()
    scaled = scaler.fit(data).transform(data)

    return scaled

###########################
# get time series in fbprophet style
hlm_vel , hlm_term  = _separate_vars(_get_helheim_data())
rink_vel, rink_term = _separate_vars(_get_rink_data())

hlm_vel_trend = fit_prophet(hlm_vel)
hlm_term_trend = fit_prophet(hlm_term)

rink_vel_trend = fit_prophet(rink_vel)
rink_term_trend = fit_prophet(rink_term)

##############
## scale stuff
##############

scaler = MinMaxScaler()
scaler.transform(data)











