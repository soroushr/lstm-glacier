import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os, sys, math
from sklearn.preprocessing import RobustScaler, MinMaxScaler
from sklearn.metrics import mean_squared_error
import pandas as pd
import tensorflow as tf
from datetime import datetime
from util import preprocess_data, create_dataset
#tf.random.set_seed(131)
plt.rc('font', family='serif')

###########################
####### DEFINE PATHS
###########################
DATA_DIR = "./data/"

FIG_DIR = "./figs-rink/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

LOG_DIR = FIG_DIR + "./logs/"
if not os.path.exists(LOG_DIR):
    os.mkdir(LOG_DIR)

###########################
####### DATA PREP
###########################
RUNOFF_NAME = "runoff_basin_5_RinkIsbrae.txt"
runoff = pd.read_csv(DATA_DIR + RUNOFF_NAME, header=0, delim_whitespace=True)
runoff.columns = ['ds', 'runoff']

VEL_NAME = "17_terminus_velocity.xlsx"
xls = pd.ExcelFile(DATA_DIR + VEL_NAME)
vel = pd.read_excel(xls, "date_vel")
term = pd.read_excel(xls, "date_terminus")

# resample data to have uniform temporal frequency
RESAMPLE_FQ = '2W'
vel_resampled = preprocess_data(vel, var_column='vel', date_column='ds', resampling=RESAMPLE_FQ)
term_resampled = preprocess_data(term, var_column='term', date_column='ds', resampling=RESAMPLE_FQ)
runoff_resampled = preprocess_data(runoff, var_column='runoff', date_column='ds', resampling=RESAMPLE_FQ)

# merge the two datasets
merged_data = pd.merge_asof(vel_resampled, runoff_resampled, on='date')
merged_data = pd.merge_asof(merged_data, term_resampled, on='date')

# FIXME: Rink terminus data is incomplete
# this is just added to clip the data to
# the last measurement of terminus position
TRIM = True
if TRIM:
    merged_data = merged_data[merged_data.date < datetime(2015,10,15)]

# define the features that are intended to include in training
f_columns = ['vel',
             'runoff',
             'term']

features = merged_data[f_columns]
features.index = merged_data['date']

# FIXME: takeout one by one
features['term'] = features['term'].mean()
features['runoff'] = features['runoff'].mean()

## define new folders for saving
folder_name = '-'.join(f_columns)

FIG_DIR += folder_name + "/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

##############################################
####### TRAIN/TEST SPLIT AND PREPROCESSING 
##############################################
TRAIN_RATIO = 0.750
train_size = int(len(features) * TRAIN_RATIO)
test_size = len(features) - train_size
train, test = features.iloc[0:train_size], features.iloc[train_size:len(features)]

print(len(train), len(test))
##########
## scaling
f_transformer = RobustScaler()
vel_transformer = RobustScaler()

# FIXME: because train[['vel']] is also part of the features, a copy
# of it is created to be able to scale it back otherwise the 
# f_transformer brings back both variables.
# fix to avoid replicating the train[['vel']] transformer
train_y_copy, test_y_copy = train[['vel']].copy(), test[['vel']].copy()

f_transformer = f_transformer.fit(train[f_columns].to_numpy())
vel_transformer = vel_transformer.fit(train_y_copy)
#vel_transformer = f_transformer

train.loc[:, f_columns] = f_transformer.transform(train[f_columns].to_numpy())
train['vel'] = vel_transformer.transform(train_y_copy)
#train['vel'] = vel_transformer.transform(train[['vel']])

test.loc[:, f_columns] = f_transformer.transform(test[f_columns].to_numpy())
test['vel'] = vel_transformer.transform(test_y_copy)
#test['vel'] = vel_transformer.transform(test[['vel']])

###############
# reformulate to a supervised learning problem
if RESAMPLE_FQ=='D':
    time_steps = 360
elif RESAMPLE_FQ=='W':
    time_steps = 50
elif RESAMPLE_FQ=='2W':
    time_steps = 36
elif RESAMPLE_FQ=='M':
    time_steps = 10

# reshape to [samples, time_steps, n_features]
X_train, y_train = create_dataset(train, train['vel'], time_steps)
X_test, y_test = create_dataset(test, test['vel'], time_steps)

################
# training specs

# define log path for tensorboard
logdir = LOG_DIR + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=logdir)

model = tf.keras.Sequential()
#model.add(
#    tf.keras.layers.Conv1D(
#        filters=4, kernel_size=2,
#        strides=1, padding="causal",
#        activation="relu"
#  )
#)
model.add(
  tf.keras.layers.Bidirectional(
    tf.keras.layers.LSTM(
      units=32,
      input_shape=(X_train.shape[1], X_train.shape[2]),
      return_sequences = True
    )
  )
)
model.add(tf.keras.layers.Dropout(rate=0.5))
model.add(
  tf.keras.layers.Bidirectional(
    tf.keras.layers.LSTM(
      units=16,
      input_shape=(X_train.shape[1], X_train.shape[2])
    )
  )
)
model.add(tf.keras.layers.Dropout(rate=0.5))
model.add(tf.keras.layers.Dense(units=4))
model.add(tf.keras.layers.Dropout(rate=0.5))
model.add(tf.keras.layers.Dense(units=1))

optimizer = tf.keras.optimizers.Adam(learning_rate=0.0001,
                                     beta_1=0.9,
                                     beta_2=0.999)
#loss = tf.keras.losses.Huber()
#loss = tf.keras.losses.Hinge()
#loss = 'mean_squared_error'
loss = 'mean_absolute_error'
model.compile(loss=loss, optimizer=optimizer)

history = model.fit(
    X_train, y_train, 
    epochs=2000,
    batch_size=200,
    validation_split=0.1,
    shuffle=True,
    callbacks=[tensorboard_callback]
)

## check losses
plt.plot(history.history['loss'], label='train', alpha=0.75)
plt.plot(history.history['val_loss'], label='validation', alpha=0.75)
plt.grid(linestyle='dotted')
plt.xlabel("# epochs")
plt.ylabel("loss")
#plt.ylim([0,0.5])
plt.title(folder_name)
plt.legend(); plt.savefig(FIG_DIR + "rink-train-val-loss.png", dpi=300)
plt.clf()

## prediction
y_pred = model.predict(X_test)

# revert and scale back
y_train_inv = vel_transformer.inverse_transform(y_train.reshape(1, -1))
y_test_inv = vel_transformer.inverse_transform(y_test.reshape(1, -1))
y_pred_inv = vel_transformer.inverse_transform(y_pred)

rmse = math.sqrt(mean_squared_error(y_test_inv.reshape(-1, 1), y_pred_inv))
################
## plots results
plt.plot(np.arange(0, len(y_train)), y_train_inv.flatten(), 'g', label="history")
plt.plot(np.arange(len(y_train), len(y_train) + len(y_test)), y_test_inv.flatten(), marker='.', label="true", alpha=0.6)
plt.plot(np.arange(len(y_train), len(y_train) + len(y_test)), y_pred_inv.flatten(), 'r', label="prediction", alpha=0.6)
plt.ylabel('velocity [m/yr]')
plt.xlabel('Time Step')
plt.legend(loc='best')
plt.title(folder_name + ' RMSE: %.3f [m/yr]' % rmse)
plt.savefig(FIG_DIR + "rink-check.png", dpi=300)
plt.clf()

## plot zoomed in to validation set
#plt.plot(test.index[time_steps:], y_test_inv.flatten(), label='true', alpha=0.7, linewidth=2, marker='o', markersize=2, color='C0')
#plt.plot(test.index[time_steps:], y_pred_inv.flatten(), label='prediction', alpha=0.7, linewidth=2, marker='s', markersize=2, color='r')
plt.plot(y_test_inv.flatten(), label='true', alpha=0.7, linewidth=2, marker='o', markersize=2, color='C0')
plt.plot(y_pred_inv.flatten(), label='prediction', alpha=0.7, linewidth=2, marker='s', markersize=2, color='r')
plt.ylabel('velocity [m/yr]')
plt.xlabel('Time Step')
plt.xticks(rotation=0)
plt.title(folder_name + ' RMSE: %.3f [m/yr]' % rmse)
plt.legend(); plt.grid(linestyle='dotted')
plt.savefig(FIG_DIR + "rink-check-zoom.png", dpi=300)
plt.clf()


def make_subplots():
    fig, axs = plt.subplots(3,1)

    ## runoff
    fig_args = {'color':'grey',
                'markersize':0.1,
                'alpha':0.50,
                'linewidth':0.5,
                'label':'runoff'}

    axs[0].plot(merged_data.date, merged_data['runoff'], **fig_args)
    axs[0].plot(merged_data.date, np.ones_like(merged_data['runoff'])*merged_data['runoff'].mean(), lw=1.1, linestyle='-', color='red')
    axs[0].set_ylabel('runoff [m$^3$/s]')
    axs[0].set_title(r'Rink Isbr$\ae$')

    ## terminus
    fig_args = {'color':'grey',
                'markersize':0.1,
                'alpha':0.50,
                'linewidth':0.5}
    axs[1].plot(merged_data.date, merged_data['term'],
                  label='terminus', **fig_args)
    axs[1].plot(merged_data.date, np.ones_like(merged_data['term'])*merged_data['term'].mean(), lw=1.1, linestyle='-', color='blue')
    axs[1].set_ylabel("terminus position [m]")

    ## velocity
    fig_args = {'color':'green',
                'markersize':0.1,
                'alpha':0.99,
                'linewidth':0.5}

    axs[2].plot(merged_data.date, merged_data['vel'], **fig_args)
    axs[2].plot(merged_data.date, np.ones_like(merged_data['vel'])*merged_data['vel'].mean(), lw=1.1, linestyle='dotted', color='grey')
    axs[2].plot(test.index[time_steps:], y_pred_inv.flatten(), label='prediction: RMSE=%.3f [m/yr]' % rmse, alpha=0.7, linewidth=2, color='k')
    axs[2].axvspan(test.index[time_steps], test.index[-1], alpha=0.1, color='green')
    axs[2].set_ylabel("velocity [m/yr]")
    axs[2].legend(loc='best', prop={'size': 6})

    left   = 0.08  # the left side of the subplots of the figure
    right  = 0.96  # the right side of the subplots of the figure
    bottom = 0.05  # the bottom of the subplots of the figure
    top    = 0.95  # the top of the subplots of the figure
    wspace = 0.30  # the amount of width reserved for space between subplots,
                   # expressed as a fraction of the average axis width
    hspace = 0.1   # the amount of height reserved for space between subplots,
                   # expressed as a fraction of the average axis height

    plt.subplots_adjust(left=left, right=right, bottom=bottom, top=top, wspace=wspace, hspace=hspace)

    for axis in axs:
        axis.axvline(test.index[time_steps], color='k', lw=1, linestyle='dashed')
 
   # Setting the values for all axes.
    plt.setp(axs, xlim=[pd.Timestamp('2000-01-01'), pd.Timestamp('2016-01-01')])
    plt.tight_layout()
    plt.savefig(FIG_DIR + "subplots.png", dpi=300)
    plt.clf()

make_subplots()
