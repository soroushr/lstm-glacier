import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os, sys
import pickle
import itertools
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import pandas as pd
from datetime import datetime
from util import preprocess_data, create_dataset
from fbprophet import Prophet
from tslearn.metrics import dtw
import seaborn as sns

plt.rc('font', family='serif')

names_dict = {'glacier9':'Store Gletscher',#
             'glacier10':'Lille Glacier',
             'glacier11':'Sermilik',#
             'glacier12':'Kangilleq',#
             'glacier13':'Sermeq Silardleq',#
             'glacier15':'Kangerluarsuup Sermia',#
             'glacier16':'Kangerdlugssup Sermerssua',#
             'glacier17':r'Rink Isbr$\ae$',#
             'glacier18':r'Umiamiko Isbr$\ae$',#
             'glacier19':r'Ingia Isbr$\ae$',#
            }

###########################
####### DEFINE PATHS
###########################
DATA_DIR = "./data/ROI_glaciers/"

FIG_DIR = "./figs-ROI/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

###########################
####### DATA PREP
###########################
def _get_comm_ids(xls1, xls2):
    data1 = pd.ExcelFile(xls1)
    data2 = pd.ExcelFile(xls2)
    
    sheets1 = set(data1.sheet_names)
    sheets2 = set(data2.sheet_names)

    lst = list(sheets1.intersection(sheets2))
    lst.sort()

    return lst


def fit_prophet(data):
    # returns a tuple with two components
    # first is date, second is trend values

    model=Prophet(interval_width=0.95) # by default is 80%
    model.fit(data)
    # forecast set to 0
    future = model.make_future_dataframe(periods=0)
    forecast = model.predict(future)
    forecast.index = pd.to_datetime(forecast.ds)

    # extract long term trend
    lines = model.plot_components(forecast)

    _trend = lines.get_axes()[0]
    trend = _trend.lines[0].get_data()
    plt.clf()

    return trend


def save_prophet(clip=False):
    TERM_FILE = DATA_DIR + "ROI_glacier_termini.xlsx"
    VEL_FILE = DATA_DIR + "ROI_glacier_velocity.xlsx"

    _term = pd.ExcelFile(TERM_FILE)
    _vel = pd.ExcelFile(VEL_FILE)

    names = _get_comm_ids(TERM_FILE, VEL_FILE)

    for name in names:
        term = pd.read_excel(_term, name)
        vel  = pd.read_excel(_vel, name)

        if clip:
            term.index = pd.to_datetime(term['ds'])
            vel.index = pd.to_datetime(vel['ds'])

            term = term[(term.index > datetime(2000,1,1)) & (term.index < datetime(2016,1,1))]
            vel = vel[(vel.index > datetime(2000,1,1)) & (vel.index < datetime(2016,1,1))]

        term_trend = fit_prophet(term)
        vel_trend = fit_prophet(vel)

        with open(FIG_DIR+name+'-vel.pickle', 'wb') as f:
            pickle.dump(vel_trend, f)

        with open(FIG_DIR+name+'-term.pickle', 'wb') as f:
            pickle.dump(term_trend, f)


def tuple_dict_to_df(dic):
    ser = pd.Series(list(dic.values()),
                    index=pd.MultiIndex.from_tuples(dic.keys()))
    df = ser.unstack()#.fillna(0)
    
    return df


def normalize_trend(trend):
    scaler = MinMaxScaler().fit(trend[1].reshape(-1, 1))
    norm = scaler.transform(trend[1].reshape(-1,1))

    trend_norm = tuple([trend[0], norm])

    return trend_norm
    
###########################
####### PLOT TIME SERIES
###########################
def plot_time_series(combined=False, normalize=False):
    TERM_FILE = DATA_DIR + "ROI_glacier_termini.xlsx"
    VEL_FILE = DATA_DIR + "ROI_glacier_velocity.xlsx"

    _term = pd.ExcelFile(TERM_FILE)
    _vel = pd.ExcelFile(VEL_FILE)

    names = _get_comm_ids(TERM_FILE, VEL_FILE)

    color = plt.cm.tab10( np.linspace(0,1,len(names)) )

    for _idx, name in enumerate(names):

        print(names_dict[name])
        print("##############")

        term = pd.read_excel(_term, name)
        vel  = pd.read_excel(_vel, name)

        with open(FIG_DIR + name + '-term.pickle', 'rb') as _f:
             term_trend = pickle.load(_f)

        with open(FIG_DIR + name + '-vel.pickle', 'rb') as _g:
             vel_trend = pickle.load(_g)

        if normalize:
            term_trend = normalize_trend(term_trend)
            vel_trend = normalize_trend(vel_trend)

        if combined:
            plt.subplot(211)
#            plt.plot(vel.ds, vel.y, '--s', markersize=0.44, lw=0.0, alpha=0.355, c=color[_idx])
            plt.plot(vel_trend[0], vel_trend[1], '-', lw=1.888, alpha=0.8, c=color[_idx])
            plt.ylabel("velocity")
#            plt.ylim([-100, 8000])
            plt.setp(plt.gca(), xlim=[pd.Timestamp('2000-01-01'), pd.Timestamp('2016-01-01')])

            plt.subplot(212)
#            plt.plot(term.ds, term.y, '--s', markersize=0.44, lw=0.0, alpha=0.355, c=color[_idx])
            plt.plot(term_trend[0], term_trend[1], '-', lw=1.888, alpha=0.8, label=names_dict[name], c=color[_idx])
            plt.xlim()
            plt.legend(loc='best', prop={'size': 5}, ncol=2)
            plt.ylabel("terminus")
            plt.setp(plt.gca(), xlim=[pd.Timestamp('2000-01-01'), pd.Timestamp('2016-01-01')])

            plt.savefig(FIG_DIR+"ALL-subplots-norm.png", dpi=300)

        else:
            plt.subplot(211)
            plt.title(names_dict[name])
            plt.plot(vel.ds, vel.y, '--s', markersize=1.5, lw=0.5, c='gray', alpha=0.6)
            plt.plot(vel_trend[0], vel_trend[1], '-', lw=2.5, c='k', alpha=0.8)
            plt.ylabel("velocity")

            plt.subplot(212)
            plt.plot(term.ds, term.y, '--s', markersize=1.5, lw=0.5, c='gray', alpha=0.6)
            plt.plot(term_trend[0], term_trend[1], '-', lw=2.5, c='k', alpha=0.8)
            plt.ylabel("terminus")

            plt.savefig(FIG_DIR+"{}.png".format(name), dpi=300)

            plt.clf()


def plot_similarity_matrix():
    TERM_FILE = DATA_DIR + "ROI_glacier_termini.xlsx"
    VEL_FILE = DATA_DIR + "ROI_glacier_velocity.xlsx"

    _term = pd.ExcelFile(TERM_FILE)
    _vel = pd.ExcelFile(VEL_FILE)

    names = _get_comm_ids(TERM_FILE, VEL_FILE)

    dtw_vel = {}
    dtw_term = {}

#    for item in set(itertools.combinations(names, 2)):
    for item in list(itertools.product(names, repeat=2)):
        name1, name2 = item[0], item[1]
        
        with open(FIG_DIR + name1 + '-term.pickle', 'rb') as _f1:
             term_trend1 = pickle.load(_f1)
        with open(FIG_DIR + name2 + '-term.pickle', 'rb') as _f2:
             term_trend2 = pickle.load(_f2)

        with open(FIG_DIR + name1 + '-vel.pickle', 'rb') as _g1:
             vel_trend1 = pickle.load(_g1)
        with open(FIG_DIR + name2 + '-vel.pickle', 'rb') as _g2:
             vel_trend2 = pickle.load(_g2)

        term_trend1 = normalize_trend(term_trend1)
        term_trend2 = normalize_trend(term_trend2)
        vel_trend1 = normalize_trend(vel_trend1)
        vel_trend2 = normalize_trend(vel_trend2)

        term_score = dtw(term_trend1[1], term_trend2[1])
        vel_score = dtw(vel_trend1[1], vel_trend2[1])

        dtw_vel[tuple([names_dict[name1], names_dict[name2]])] = vel_score
        dtw_term[tuple([names_dict[name1], names_dict[name2]])] = term_score


    df_term = tuple_dict_to_df(dtw_term)
    df_vel = tuple_dict_to_df(dtw_vel)

    ## terminus
    res = sns.heatmap(df_term, square = True,
                         linewidths = .5,
                         cmap = 'viridis',
                         vmin=0, vmax=12,
                         annot = True,
                         annot_kws = {'size': 5})

    res.set_xticklabels(res.get_xmajorticklabels(), fontsize = 4, rotation=15)
    res.set_yticklabels(res.get_xmajorticklabels(), fontsize = 4)

    plt.title("DTW similarity for terminus (0=identical)")
    plt.savefig(FIG_DIR+"similarity-term-dtw.png", dpi=300)
    plt.clf()

    ## velocity
    res = sns.heatmap(df_vel, square = True,
                         linewidths = .5,
                         cmap = 'viridis',
                         annot = True,
                         vmin=0, vmax=12,
                         annot_kws = {'size': 5})

    res.set_xticklabels(res.get_xmajorticklabels(), fontsize = 4, rotation=15)
    res.set_yticklabels(res.get_xmajorticklabels(), fontsize = 4)

    plt.title("DTW similarity for velocity (0=identical)")
    plt.savefig(FIG_DIR+"similarity-vel-dtw.png", dpi=300)
    plt.clf()

    return dtw_vel, dtw_term, df_vel, df_term


if __name__ == "__main__": 

#    save_prophet(clip=True)
#    plot_time_series(combined=True, normalize=True)
    dtw_vel, dtw_term, df_vel, df_term = plot_similarity_matrix()
    plt.clf()




############## this works for all data without normalizing
#        else:
#            plt.subplot(211)
#            plt.plot(vel.ds, vel.y, '--s', markersize=0.44, lw=0.0, alpha=0.355, c=color[_idx])
#            plt.plot(vel_trend[0], vel_trend[1], '-', lw=2.5, alpha=0.8, c=color[_idx])
#            plt.ylabel("velocity")
#            plt.ylim([-100, 8000])
#            plt.setp(plt.gca(), xlim=[pd.Timestamp('2000-01-01'), pd.Timestamp('2016-01-01')])

#            plt.subplot(212)
#            plt.plot(term.ds, term.y, '--s', markersize=0.44, lw=0.0, alpha=0.355, c=color[_idx])
#            plt.plot(term_trend[0], term_trend[1], '-', lw=2.5, alpha=0.8, label=names_dict[name], c=color[_idx])
#            plt.xlim()
#            plt.legend(loc='best', prop={'size': 7}, ncol=2)
#            plt.ylabel("terminus")
#            plt.setp(plt.gca(), xlim=[pd.Timestamp('2000-01-01'), pd.Timestamp('2016-01-01')])

#            plt.savefig(FIG_DIR+"ALL-subplots.png", dpi=300)

#    plt.clf()

