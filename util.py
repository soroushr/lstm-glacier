import numpy as np
import pandas as pd

def preprocess_data(data, var_column='y', date_column='ds', resampling='M'):
    # TODO: add capabilities for different resampling methods
    """Preprocesses time-series data to create uniform temporal   
    frequency and fills in the gaps with a give interpolation scheme

    Args:
        data: DataFrame of time series [pd.DataFrame]
        var_column: name of the column containing variable [string]
        date_column: name of the column containing time [string]
        resampling: resampling frequency [string]
                    options: {'D', 'W', 'W', 'M'}

    Returns:
        data_resamled: DataFrame with uniform sampling frequency [pd.DataFrame]
    """
    data.index = pd.to_datetime(data[date_column])
    data.index.name = 'date'

    data_resampled = data.reset_index().set_index('date', drop=True).resample(resampling).mean()

    data_resampled[var_column] = data_resampled[var_column].interpolate(method='index')

    return data_resampled


def create_dataset(X, y, time_steps=1):
    """Takes the training and test sequences and
    returns training and test vectors

    Args:
        X: matrix containing all the features [numpy array]
        y: matrix containing all the targests [numpy array]

    Returns:
        np.array(Xs): windowed data of features
        np.array(ys): windowed data of targets
    """
    Xs, ys = [], []
    for i in range(len(X) - time_steps):
        v = X.iloc[i:(i + time_steps)].values
        Xs.append(v)
        ys.append(y.iloc[i + time_steps])

    return np.array(Xs), np.array(ys)

