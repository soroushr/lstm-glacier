import numpy as np
import pandas as pd
import os
import sys
import matplotlib
import matplotlib.pyplot as plt
from datetime import datetime
from util import preprocess_data, create_dataset

font = {'family' : 'serif',
        'size'   : 6
        }

matplotlib.rc('font', **font)

DATA_DIR = os.getcwd() + '/data/'
path = DATA_DIR
SAVE_DIR = os.getcwd() + '/figs-rink/plot-time-series/'

if not os.path.exists(SAVE_DIR):
    os.mkdir(SAVE_DIR)

###########################
####### DATA PREP
###########################
RUNOFF_NAME = "runoff_basin_5_RinkIsbrae.txt"
runoff = pd.read_csv(DATA_DIR + RUNOFF_NAME, header=0, delim_whitespace=True)
runoff.columns = ['ds', 'runoff']

VEL_NAME = "17_terminus_velocity.xlsx"
xls = pd.ExcelFile(DATA_DIR + VEL_NAME)
vel = pd.read_excel(xls, "date_vel")
term = pd.read_excel(xls, "date_terminus")

# resample data to have uniform temporal frequency
RESAMPLE_FQ = '2W'
vel_resampled = preprocess_data(vel, var_column='vel', date_column='ds', resampling=RESAMPLE_FQ)
term_resampled = preprocess_data(term, var_column='term', date_column='ds', resampling=RESAMPLE_FQ)
runoff_resampled = preprocess_data(runoff, var_column='runoff', date_column='ds', resampling=RESAMPLE_FQ)

# merge the two datasets
merged_data = pd.merge_asof(vel_resampled, runoff_resampled, on='date')
merged_data = pd.merge_asof(merged_data, term_resampled, on='date')

TRIM = True
if TRIM:
    merged_data = merged_data[merged_data.date < datetime(2015,10,15)]

###############
#### PLOTS
###############
fig, axs = plt.subplots(3,1)

## runoff
fig_args = {'color':'red',
            'markersize':0.1,
            'alpha':0.99,
            'linewidth':0.5,
            'label':'runoff'}

axs[0].plot(merged_data.date, merged_data['runoff'], **fig_args)
axs[0].set_ylabel('runoff [m$^3$/s]')


## terminus
fig_args = {'markersize':0.1,
            'alpha':0.99,
            'linewidth':0.5}

axs[1].plot(merged_data.date, merged_data['term'],
              label='terminus', **fig_args)

#axs[0,1].plot(resampled_tmin.index, np.zeros_like(resampled_tmin), lw=0.1,
#                                                                   color='gray')
axs[1].set_ylabel("terminus position [km]")
#axs[0,1].set_ylim([-30, 45])

## velocity
fig_args = {'markersize':0.1,
            'alpha':0.99,
            'linewidth':0.5,
            'color':'#004d00'}

axs[2].plot(merged_data.date, merged_data['vel'],
              label='prcp', **fig_args)
axs[2].set_ylabel("velocity [m/yr]")
#axs[2].set_ylim([-5, 100])

plt.suptitle(r'Rink Isbr$\ae$')

left   = 0.08  # the left side of the subplots of the figure
right  = 0.96  # the right side of the subplots of the figure
bottom = 0.05  # the bottom of the subplots of the figure
top    = 0.95  # the top of the subplots of the figure
wspace = 0.30  # the amount of width reserved for space between subplots,
               # expressed as a fraction of the average axis width
hspace = 0.2   # the amount of height reserved for space between subplots,
               # expressed as a fraction of the average axis height

plt.subplots_adjust(left=left, right=right, bottom=bottom, top=top, wspace=wspace, hspace=hspace)

# Setting the values for all axes.
#plt.setp(axs, xlim=[pd.Timestamp('2000-01-01'), pd.Timestamp('2018-12-30')])

plt.savefig(SAVE_DIR + "Rink.png", dpi=300)
plt.clf()

