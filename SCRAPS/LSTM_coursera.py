import tensorflow as tf
import numpy as np
import os, sys
import matplotlib.pyplot as plt
import pandas as pd
print(tf.__version__)

plt.rc('font', family='serif')

################################
FIG_DIR = "./figs/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

################################
def plot_series(time, series, format="-", start=0, end=None):
    plt.plot(time[start:end], series[start:end], format)
#    plt.xlabel("Time")
    plt.ylabel("velocity")
    plt.grid(linestyle='dotted')

################################
def preprocess_data(data, resampling='M'):
    # resample to 'M', 'W', 'D'
    data_resampled = data.reset_index().set_index('date', drop=True).resample(resampling).mean()

    # interpolate to fill in the gaps
    data_resampled['y'] = data_resampled['y'].interpolate(method='index',)# order=2)

    return data_resampled

################################
XLS_NAME = "17_terminus_velocity.xlsx"
xls = pd.ExcelFile(XLS_NAME)
data = pd.read_excel(xls, "date_vel")

data.index = pd.to_datetime(data["ds"])
data.index.name = 'date'

RESAMPLE_FQ = '2W'
data_resampled = preprocess_data(data, resampling=RESAMPLE_FQ)

plt.clf()
plt.plot(data.index, data['y'], label='data', lw=2, alpha=0.6)#, markersize=2, marker='s')
plt.plot(data_resampled.index, data_resampled['y'], label='resampled', lw=1, alpha=0.8)#, marker='o', markersize=2)
plt.legend(loc='best')
plt.savefig(FIG_DIR + 'coursera-velocity-interpolation.png', dpi=300)

time_step = np.arange(len(data_resampled.index))

sunspots = data_resampled['y']

series = np.array(sunspots)
time = np.array(time_step)

###############################
TRAIN_PERCENT = 0.8
split_time = int(TRAIN_PERCENT*len(data_resampled))
time_train = time[:split_time]
x_train = series[:split_time]
time_valid = time[split_time:]
x_valid = series[split_time:]

###############################
def windowed_dataset(series, window_size, batch_size, shuffle_buffer):
    series = tf.expand_dims(series, axis=-1)
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size + 1, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda w: w.batch(window_size + 1))
    ds = ds.shuffle(shuffle_buffer)
    ds = ds.map(lambda w: (w[:-1], w[1:]))
    return ds.batch(batch_size).prefetch(1)

###############################
def model_forecast(model, series, window_size):
    ds = tf.data.Dataset.from_tensor_slices(series)
    ds = ds.window(window_size, shift=1, drop_remainder=True)
    ds = ds.flat_map(lambda w: w.batch(window_size))
    ds = ds.batch(4).prefetch(1)
    forecast = model.predict(ds)
    return forecast

###############################
tf.keras.backend.clear_session()
tf.random.set_seed(51)
np.random.seed(51)

window_size = 64
batch_size = 32
shuffle_buffer_size = 100

train_set = windowed_dataset(x_train, window_size, batch_size, shuffle_buffer_size)
print(train_set)
print(x_train.shape)

model = tf.keras.models.Sequential([
  tf.keras.layers.Conv1D(filters=16, kernel_size=5,
                      strides=1, padding="causal",
                      activation="relu",
                      input_shape=[None, 1]),
#  tf.keras.layers.LSTM(64, return_sequences=True),
  tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(8, return_sequences=True)),
#  tf.keras.layers.LSTM(64, return_sequences=True),
#  tf.keras.layers.Dense(30, activation="relu"),
  tf.keras.layers.Dense(10, activation="relu"),
  tf.keras.layers.Dense(1),
  tf.keras.layers.Lambda(lambda x: x * 1000)
])

lr_schedule = tf.keras.callbacks.LearningRateScheduler(
    lambda epoch: 1e-8 * 10**(epoch / 20))

optimizer = tf.keras.optimizers.SGD(lr=1e-5, momentum=0.9)

model.compile(loss = tf.keras.losses.Huber(),
              optimizer = optimizer,
              metrics = ["mae"])

history = model.fit(train_set, epochs=50, callbacks=[lr_schedule])

#####################################################
# plot the los history as a function of learning rate
plt.clf()
plt.semilogx(history.history["lr"], history.history["loss"])
plt.ylabel("loss")
plt.xlabel("learning rate")
plt.grid(linestyle='dotted')
plt.savefig(FIG_DIR + 'coursera-learning-rate.png', dpi=300)

"""
plt.clf()
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.legend()
plt.grid(linestyle='dotted')
plt.title("train and test losses")
plt.savefig(FIG_DIR + 'coursera-velocity-loss.png', dpi=300)
"""

#####################################################
# forceast
rnn_forecast = model_forecast(model, series[..., np.newaxis], window_size)
rnn_forecast = rnn_forecast[split_time - window_size:-1, -1, 0]
plt.clf()
plot_series(data_resampled.index[split_time:], x_valid)#, label='data')
plot_series(data_resampled.index[split_time:], rnn_forecast)#, label='forecast')
plt.xticks(rotation=15)
plt.savefig(FIG_DIR + 'coursera-forecast.png', dpi=300)

