import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
import os, sys
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense, LSTM, Conv1D, Dropout
from keras import optimizers
plt.rc('font', family='serif')

tf.keras.backend.clear_session()

FIG_DIR = "./figs/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

XLS_NAME = "17_terminus_velocity.xlsx"
xls = pd.ExcelFile(XLS_NAME)
vel = pd.read_excel(xls, "date_vel")
term = pd.read_excel(xls, "date_terminus")

vel.index = pd.to_datetime(vel["ds"])
vel.index.name = 'date'

def preprocess_data(data, resampling='M'):
    # resample to 'M', 'W', 'D'
    data_resampled = data.reset_index().set_index('date', drop=True).resample(resampling).mean()

    # interpolate to fill in the gaps
    #data_resampled['y'] = data_resampled['y'].interpolate(method='from_derivatives', limit_direction='both', limit=20)
    data_resampled['y'] = data_resampled['y'].interpolate(method='index')

    return data_resampled

# resample data to have uniform temporal frequency
RESAMPLE_FQ = '2W'
data_resampled = preprocess_data(vel, resampling=RESAMPLE_FQ)

# plot and check the resampling against actual data
plt.clf()
plt.plot(vel.index, vel['y'], label='data', lw=2, alpha=0.6)
plt.plot(data_resampled.index, data_resampled['y'], lw=1, label='resampled')
plt.legend(loc='best')
plt.savefig(FIG_DIR + "rink-vel-resampled.png", dpi=300)

# convert series to supervised learning
def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
    """
    Frame a time series as a supervised learning dataset.
    Arguments:
        data: Sequence of observations as a list or NumPy array.
        n_in: Number of lag observations as input (X).
        n_out: Number of observations as output (y).
        dropnan: Boolean whether or not to drop rows with NaN values.
    Returns:
        Pandas DataFrame of series framed for supervised learning.
    """
    n_vars = 1 if type(data) is list else data.shape[1]
    df = pd.DataFrame(data)
    cols, names = list(), list()
    # input sequence (t-n, ... t-1)
    for i in range(n_in, 0, -1):
        cols.append(df.shift(i))
        names += [('var%d(t-%d)' % (j+1, i)) for j in range(n_vars)]
    # forecast sequence (t, t+1, ... t+n)
    for i in range(0, n_out):
        cols.append(df.shift(-i))
        if i == 0:
            names += [('var%d(t)' % (j+1)) for j in range(n_vars)]
        else:
            names += [('var%d(t+%d)' % (j+1, i)) for j in range(n_vars)]
    # put it all together
    agg = pd.concat(cols, axis=1)
    agg.columns = names
    # drop rows with NaN values
    if dropnan:
        agg.dropna(inplace=True)
    return agg


###################
###################
###################
values = data_resampled.values
values = values.astype('float32')
scaler = MinMaxScaler(feature_range=(0, 1))
scaled = scaler.fit_transform(values)

N_IN  = 1
N_OUT = 1
reframed = series_to_supervised(scaled, n_in=N_IN, n_out=N_OUT)
print(reframed.head())

#to_drop = list(range(reframed.shape[1]-1, values.shape[1], -1))
to_drop = list(range(1,reframed.shape[1]-1))
reframed.drop(reframed.columns[to_drop], axis=1, inplace=True)

print(reframed.head())

#################
values = reframed.values
PERCENT_TRAIN = 0.7
n_train_samples = int(PERCENT_TRAIN*values.shape[0])
train = values[:n_train_samples, :]
test = values[n_train_samples:, :]
# split into input and outputs
train_X, train_y = train[:, :-N_OUT], train[:, -N_OUT]
test_X ,  test_y = test[:, :-N_OUT] , test[:, -N_OUT]
# reshape input to be 3D [samples, timesteps, features]
train_X = train_X.reshape((train_X.shape[0], 1, train_X.shape[1]))
test_X  = test_X.reshape((test_X.shape[0], 1, test_X.shape[1]))
print(train_X.shape, train_y.shape, test_X.shape, test_y.shape)

###########
# design network
model = Sequential()
model.add(Conv1D(filters=16, kernel_size=4, strides=1,
                  padding='causal', activation='relu',
                  input_shape = [None, train_X.shape[2]] ))

model.add(LSTM(3, input_shape = (train_X.shape[1], train_X.shape[2]), return_sequences=False) )
#model.add(Dropout(0.5))

#model.add(LSTM(8, input_shape = (train_X.shape[1], train_X.shape[2]), return_sequences=True) )
#model.add(LSTM(4, input_shape = (train_X.shape[1], train_X.shape[2])) )
#model.add(Dense(32, activation='relu'))
#model.add(Dense(4, activation='relu'))
#model.add(Dropout(0.5))
model.add(Dense(1))
#model.add(Dropout(0.8))
optimizer = optimizers.Adam(lr=1e-4, decay=1e-8)
model.compile(loss='mae', optimizer=optimizer)

# fit network
EPOCHS = 1000
history = model.fit(train_X, train_y, epochs=EPOCHS, batch_size=16, validation_data=(test_X, test_y), verbose=1, shuffle=True)


# plot history
plt.clf()
plt.plot(history.history['loss'], label='train')
plt.plot(history.history['val_loss'], label='test')
plt.legend()
#plt.ylim([0,0.1])
plt.grid(linestyle='dotted')
plt.title("train and test losses")
plt.savefig(FIG_DIR + 'rink-velocity-loss.png', dpi=300)

##########
# make a prediction
yhat   = model.predict(test_X)
test_X = test_X.reshape((test_X.shape[0], test_X.shape[2]))
# invert scaling for forecast
inv_yhat = np.concatenate((yhat, test_X[:, 1:]), axis=1)
print("inv_yhat shape is: ", inv_yhat.shape)
inv_yhat = scaler.inverse_transform(inv_yhat)
inv_yhat = inv_yhat[:,0]
# invert scaling for actual
test_y = test_y.reshape((len(test_y), 1))
inv_y  = np.concatenate((test_y, test_X[:, 1:]), axis=1)
inv_y  = scaler.inverse_transform(inv_y)
inv_y  = inv_y[:,0]
# calculate RMSE
rmse = math.sqrt(mean_squared_error(inv_y, inv_yhat))
print('Test RMSE: %.3f' % rmse)

#############################
## plot the test and forecast
plt.clf()
plt.plot(data_resampled.index[-inv_y.shape[0]:], inv_y, label='test', alpha=0.7, linewidth=2, marker='o', markersize=2)
plt.plot(data_resampled.index[-inv_y.shape[0]:], inv_yhat, label='forecast', alpha=0.7, linewidth=2, marker='s', markersize=2)
#plt.ylim([2200, 4900])
#plt.xlim([pd.Timestamp('2018-01-01'), pd.Timestamp('2018-06-01')])
plt.legend(loc='best')
plt.xticks(rotation=15)
plt.grid(linestyle='dotted')
plt.ylabel("velocity [m/yr]")
plt.title('RMSE: %.3f [m/yr]' % rmse)
plt.savefig(FIG_DIR + 'rink-velocity.png', dpi=300)

