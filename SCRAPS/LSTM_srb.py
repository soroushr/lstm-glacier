import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os, sys
import pandas as pd
import tensorflow as tf

tf.random.set_seed(13)
plt.rc('font', family='serif')

tf.keras.backend.clear_session()

FIG_DIR = "./figs/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

XLS_NAME = "17_terminus_velocity.xlsx"
xls = pd.ExcelFile(XLS_NAME)
vel = pd.read_excel(xls, "date_vel")
term = pd.read_excel(xls, "date_terminus")

vel.index = pd.to_datetime(vel["ds"])
vel.index.name = 'date'


def preprocess_data(data, resampling='M'):
    # resample to 'M', 'W', 'D'
    data_resampled = data.reset_index().set_index('date', drop=True).resample(resampling).mean()

    # interpolate to fill in the gaps
    #data_resampled['y'] = data_resampled['y'].interpolate(method='from_derivatives', limit_direction='both', limit=20)
    data_resampled['y'] = data_resampled['y'].interpolate(method='index')

    return data_resampled

# resample data to have uniform temporal frequency
RESAMPLE_FQ = '2W'
data_resampled = preprocess_data(vel, resampling=RESAMPLE_FQ)


###############################
###### FUNCTIONS
###############################
def create_time_steps(length):

    return list(range(-length, 0))


def show_plot(plot_data, delta, title):
    labels = ['History', 'True Future', 'Model Prediction']
    marker = ['.-', 'rx', 'go']
    time_steps = create_time_steps(plot_data[0].shape[0])
    if delta:
        future = delta
    else:
        future = 0

    plt.title(title)
    for i, x in enumerate(plot_data):
        if i:
            plt.plot(future, plot_data[i], marker[i], markersize=10, label=labels[i])
        else:
            plt.plot(time_steps, plot_data[i].flatten(), marker[i], label=labels[i])
    plt.legend()
    plt.xlim([time_steps[0], (future+5)*2])
    plt.xlabel('Time-Step')

    return plt


def baseline(history):

    return np.mean(history)


def windowed_data(dataset, target, start_index, end_index,
                        history_size, target_size,
                        step = 1, single_step=False):
    data = []
    labels = []

    start_index = start_index + history_size
    if end_index is None:
        end_index = len(dataset) - target_size

    for i in range(start_index, end_index):
        indices = range(i-history_size, i, step)
        data.append(dataset[indices])

    if single_step:
        labels.append(target[i+target_size])
    else:
        labels.append(target[i:i+target_size])

    return np.array(data), np.array(labels)

#    data = []
#    labels = []

#    start_index = start_index + history_size
#    if end_index is None:
#        end_index = len(dataset) - target_size

#    for i in range(start_index, end_index):
#        indices = range(i-history_size, i)
#        # Reshape data from (history_size,) to (history_size, 1)
#        data.append(np.reshape(dataset[indices], (history_size, 1)))
#        labels.append(dataset[i+target_size])

#    return np.array(data), np.array(labels)
###############################
###############################

input_data = pd.DataFrame(np.arange(0,200))# #data_resampled['y']
#input_data.index = data_resampled.index

input_data = input_data.values

TRAIN_SPLIT = int(len(input_data)*0.8)

# standardization
train_mean = input_data[:TRAIN_SPLIT].mean()
train_std = input_data[:TRAIN_SPLIT].std()

input_data = (input_data-train_mean)/train_std


past_history = 20
future_target = 0

x_train, y_train = windowed_data(dataset      = input_data,
                                 start_index  = 0,
                                 end_index    = TRAIN_SPLIT,
                                 history_size = past_history,
                                 target_size  = future_target)

x_val, y_val = windowed_data(dataset      = input_data,
                             start_index  = TRAIN_SPLIT,
                             end_index    = None,
                             history_size = past_history,
                             target_size  = future_target)

plot = True
if plot:
    show_plot([x_train[0], y_train[0], baseline(x_train[0])], 0,
               'Baseline Prediction Example')
    plt.savefig(FIG_DIR + 'tf-baseline.png', dpi=300)
    plt.clf()

##############
## set up LSTM
BATCH_SIZE = 5
BUFFER_SIZE = 100

train_tf = tf.data.Dataset.from_tensor_slices((x_train, y_train))
train_tf = train_tf.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

val_tf = tf.data.Dataset.from_tensor_slices((x_val, y_val))
val_tf = val_tf.batch(BATCH_SIZE).repeat()

simple_lstm_model = tf.keras.models.Sequential([
    tf.keras.layers.LSTM(8, input_shape=x_train.shape[-2:]),
    tf.keras.layers.Dense(1)
])

simple_lstm_model.compile(optimizer='adam', loss='mae')

EVALUATION_INTERVAL = 200
EPOCHS = 10

# train LSTM
simple_lstm_model.fit(train_tf, epochs=EPOCHS,
                      steps_per_epoch=EVALUATION_INTERVAL,
                      validation_data=val_tf, validation_steps=50)


for x, y in train_tf.take(3):
    plot = show_plot([x[0].numpy(), y[0].numpy(),
                    simple_lstm_model.predict(x)[0]], 0, 'Simple LSTM model')
    plot.show()#savefig(FIG_DIR + 'tf-test-prediction.png', dpi=300)
