import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os, sys
from sklearn.preprocessing import RobustScaler, MinMaxScaler
import pandas as pd
import tensorflow as tf

tf.random.set_seed(13)
plt.rc('font', family='serif')

FIG_DIR = "./figs-rink/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

###########################
####### DATA PREP
###########################
runoff = pd.read_csv("runoff_basin_5_RinkIsbrae.txt", header=0, delim_whitespace=True)
runoff.columns = ['ds', 'runoff']
runoff.index = pd.to_datetime(runoff['ds'])
runoff.index.name = 'date'

XLS_NAME = "17_terminus_velocity.xlsx"
xls = pd.ExcelFile(XLS_NAME)
vel = pd.read_excel(xls, "date_vel")
term = pd.read_excel(xls, "date_terminus")

vel.index = pd.to_datetime(vel["ds"])
vel.index.name = 'date'


def preprocess_data(data, var='y', resampling='M'):
    # resample to 'M', 'W', 'D'
    data_resampled = data.reset_index().set_index('date', drop=True).resample(resampling).mean()

    # interpolate to fill in the gaps
    #data_resampled['y'] = data_resampled['y'].interpolate(method='from_derivatives', limit_direction='both', limit=20)
    data_resampled[var] = data_resampled[var].interpolate(method='index')

    return data_resampled

# resample data to have uniform temporal frequency
RESAMPLE_FQ = 'W'
vel_resampled = preprocess_data(vel, resampling=RESAMPLE_FQ)
runoff_resampled = preprocess_data(runoff, var='runoff', resampling=RESAMPLE_FQ)

df = pd.merge_asof(vel_resampled, runoff_resampled, on='date')

f_columns = ['y', 'runoff']

features = df[f_columns]
features.index = df['date']

##############################################
####### TRAIN/TEST AND PREPROCESSING
##############################################
train_size = int(len(features) * 0.75)
test_size = len(features) - train_size
train, test = features.iloc[0:train_size], features.iloc[train_size:len(features)]

print(len(train), len(test))

def create_dataset(X, y, time_steps=1):
    Xs, ys = [], []
    for i in range(len(X) - time_steps):
        v = X.iloc[i:(i + time_steps)].values
        Xs.append(v)
        ys.append(y.iloc[i + time_steps])

    return np.array(Xs), np.array(ys)

##########
## scaling
f_transformer = RobustScaler()
vel_transformer = RobustScaler()

train_y_copy, test_y_copy = train[['y']].copy(), test[['y']].copy()

f_transformer = f_transformer.fit(train[f_columns].to_numpy())
vel_transformer = vel_transformer.fit(train_y_copy)
#vel_transformer = f_transformer

train.loc[:, f_columns] = f_transformer.transform(train[f_columns].to_numpy())
train['y'] = vel_transformer.transform(train_y_copy)
#train['y'] = vel_transformer.transform(train[['y']])

test.loc[:, f_columns] = f_transformer.transform(test[f_columns].to_numpy())
test['y'] = vel_transformer.transform(test_y_copy)
#test['y'] = vel_transformer.transform(test[['y']])


###############
# reformulate to a supervised learning problem
time_steps = 30

# reshape to [samples, time_steps, n_features]
X_train, y_train = create_dataset(train, train['y'], time_steps)
X_test, y_test = create_dataset(test, test['y'], time_steps)

print(X_train.shape, y_train.shape)

model = tf.keras.Sequential()
model.add(
    tf.keras.layers.Conv1D(
        filters=6, kernel_size=5,
        strides=1, padding="causal",
        activation="relu"
  )
)
model.add(
  tf.keras.layers.Bidirectional(
    tf.keras.layers.LSTM(
      units=4, 
      input_shape=(X_train.shape[1], X_train.shape[2])
    )
  )
)
model.add(tf.keras.layers.Dropout(rate=0.2))
model.add(tf.keras.layers.Dense(units=32))
model.add(tf.keras.layers.Dropout(rate=0.2))
model.add(tf.keras.layers.Dense(units=1))

optimizer = tf.keras.optimizers.Adam(learning_rate=0.001,
                                     beta_1=0.9,
                                     beta_2=0.999)
model.compile(loss='mean_squared_error', optimizer=optimizer)

history = model.fit(
    X_train, y_train, 
    epochs=1000, 
    batch_size=100,
    validation_split=0.1,
    shuffle=False
)

## check losses
plt.plot(history.history['loss'], label='train', alpha=0.75)
plt.plot(history.history['val_loss'], label='validation', alpha=0.75)
plt.grid(linestyle='dotted')
plt.xlabel("# epochs")
plt.ylabel("loss")
plt.ylim([0,0.5])
plt.legend(); plt.savefig(FIG_DIR + "rink-train-val-loss.png", dpi=300)
plt.clf()

## prediction
y_pred = model.predict(X_test)

# revert and scale back
y_train_inv = vel_transformer.inverse_transform(y_train.reshape(1, -1))
y_test_inv = vel_transformer.inverse_transform(y_test.reshape(1, -1))
y_pred_inv = vel_transformer.inverse_transform(y_pred)


################
## plots results
plt.plot(np.arange(0, len(y_train)), y_train_inv.flatten(), 'g', label="history")
plt.plot(np.arange(len(y_train), len(y_train) + len(y_test)), y_test_inv.flatten(), marker='.', label="true", alpha=0.6)
plt.plot(np.arange(len(y_train), len(y_train) + len(y_test)), y_pred_inv.flatten(), 'r', label="prediction", alpha=0.6)
plt.ylabel('velocity [m/yr]')
plt.xlabel('Time Step')
plt.legend(loc='best')
plt.savefig(FIG_DIR + "rink-check.png", dpi=300)
plt.clf()

## plot zoomed in to validation set
plt.plot(test.index[time_steps:], y_test_inv.flatten(), label='true', alpha=0.7, linewidth=2, marker='o', markersize=2, color='C0')
plt.plot(test.index[time_steps:], y_pred_inv.flatten(), label='prediction', alpha=0.7, linewidth=2, marker='s', markersize=2, color='r')
#plt.plot(y_test_inv.flatten(), label='true', alpha=0.7, linewidth=2, marker='o', markersize=2, color='C0')
#plt.plot(y_pred_inv.flatten(), label='prediction', alpha=0.7, linewidth=2, marker='s', markersize=2, color='r')
plt.ylabel('velocity [m/yr]')
plt.xlabel('Time Step')
plt.xticks(rotation=15)
plt.legend(); plt.grid(linestyle='dotted')
plt.savefig(FIG_DIR + "rink-check-zoom.png", dpi=300)
plt.clf()

