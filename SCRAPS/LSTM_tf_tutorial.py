import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
import tensorflow as tf

tf.random.set_seed(13)
plt.rc('font', family='serif')

UNIVARIATE = True
MULTIVARIATE = False

FIG_DIR = "./figs/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

CSV_PATH = os.getcwd() + "/jena-climate-2009-2016/jena_climate_2009_2016.csv"

df = pd.read_csv(CSV_PATH)

## create windows for training
def univariate_data(dataset, start_index, end_index, history_size, target_size):
    data = []
    labels = []

    start_index = start_index + history_size
    if end_index is None:
        end_index = len(dataset) - target_size

    for i in range(start_index, end_index):
        indices = range(i-history_size, i)
        # Reshape data from (history_size,) to (history_size, 1)
        data.append(np.reshape(dataset[indices], (history_size, 1)))
        labels.append(dataset[i+target_size])

    return np.array(data), np.array(labels)

TRAIN_SPLIT = 300000

if UNIVARIATE:
    ## get temperature
    uni_data = df['T (degC)']
    uni_data.index = df['Date Time']

    uni_data = uni_data.values

    # standardization
    uni_train_mean = uni_data[:TRAIN_SPLIT].mean()
    uni_train_std = uni_data[:TRAIN_SPLIT].std()

    uni_data = (uni_data-uni_train_mean)/uni_train_std


    univariate_past_history = 20
    univariate_future_target = 0

    x_train_uni, y_train_uni = univariate_data(dataset      = uni_data,
                                               start_index  = 0,
                                               end_index    = TRAIN_SPLIT,
                                               history_size = univariate_past_history,
                                               target_size  = univariate_future_target)

    x_val_uni, y_val_uni = univariate_data(dataset      = uni_data,
                                           start_index  = TRAIN_SPLIT,
                                           end_index    = None,
                                           history_size = univariate_past_history,
                                           target_size  = univariate_future_target)

    """
    def create_time_steps(length):
        return list(range(-length, 0))


    def show_plot(plot_data, delta, title):
        labels = ['History', 'True Future', 'Model Prediction']
        marker = ['.-', 'rx', 'go']
        time_steps = create_time_steps(plot_data[0].shape[0])
        if delta:
            future = delta
        else:
            future = 0

        plt.title(title)
        for i, x in enumerate(plot_data):
            if i:
                plt.plot(future, plot_data[i], marker[i], markersize=10, label=labels[i])
            else:
                plt.plot(time_steps, plot_data[i].flatten(), marker[i], label=labels[i])
        plt.legend()
        plt.xlim([time_steps[0], (future+5)*2])
        plt.xlabel('Time-Step')
        return plt

    def baseline(history):
        return np.mean(history)

    plot = False
    if plot:
        show_plot([x_train_uni[0], y_train_uni[0], baseline(x_train_uni[0])], 0,
                   'Baseline Prediction Example')
        plt.savefig(FIG_DIR + 'tf-baseline.png', dpi=300)
        plt.clf()

    ##############
    ## set up LSTM
    BATCH_SIZE = 256
    BUFFER_SIZE = 10000

    train_univariate = tf.data.Dataset.from_tensor_slices((x_train_uni, y_train_uni))
    train_univariate = train_univariate.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

    val_univariate = tf.data.Dataset.from_tensor_slices((x_val_uni, y_val_uni))
    val_univariate = val_univariate.batch(BATCH_SIZE).repeat()

    simple_lstm_model = tf.keras.models.Sequential([
        tf.keras.layers.LSTM(8, input_shape=x_train_uni.shape[-2:]),
        tf.keras.layers.Dense(1)
    ])

    simple_lstm_model.compile(optimizer='adam', loss='mae')

    EVALUATION_INTERVAL = 200
    EPOCHS = 10

    # train LSTM
    simple_lstm_model.fit(train_univariate, epochs=EPOCHS,
                          steps_per_epoch=EVALUATION_INTERVAL,
                          validation_data=val_univariate, validation_steps=50)
    """







"""
if MULTIVARIATE:
    features_considered = ['p (mbar)', 'T (degC)', 'rho (g/m**3)']
    
    features = df[features_considered]
    features.index = df['Date Time']

    dataset = features.values
    data_mean = dataset[:TRAIN_SPLIT].mean(axis=0)
    data_std = dataset[:TRAIN_SPLIT].std(axis=0)

    dataset = (dataset-data_mean)/data_std

    def multivariate_data(dataset, target, start_index, end_index, history_size,
                          target_size, step, single_step=False):
        data = []
        labels = []

        start_index = start_index + history_size
        if end_index is None:
            end_index = len(dataset) - target_size

        for i in range(start_index, end_index):
            indices = range(i-history_size, i, step)
            data.append(dataset[indices])

            if single_step:
                labels.append(target[i+target_size])
            else:
                labels.append(target[i:i+target_size])

        return np.array(data), np.array(labels)

    # train/val set up
    past_history = 720
    future_target = 72
    STEP = 6

    x_train_single, y_train_single = multivariate_data(dataset, dataset[:, 1], 0,
                                                       TRAIN_SPLIT, past_history,
                                                       future_target, STEP,
                                                       single_step=True)
    x_val_single, y_val_single = multivariate_data(dataset, dataset[:, 1],
                                                   TRAIN_SPLIT, None, past_history,
                                                   future_target, STEP,
                                                   single_step=True)
"""
