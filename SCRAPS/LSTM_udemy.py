import numpy as np
import pandas as pd
import tensorflow as tf
import matplotlib.pyplot as plt
import os, sys
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras.models import Sequential
from keras.layers import Dense, LSTM, Conv1D, Dropout
from keras import optimizers
plt.rc('font', family='serif')

tf.keras.backend.clear_session()

FIG_DIR = "./figs/"
if not os.path.exists(FIG_DIR):
    os.mkdir(FIG_DIR)

XLS_NAME = "17_terminus_velocity.xlsx"
xls = pd.ExcelFile(XLS_NAME)
vel = pd.read_excel(xls, "date_vel")
term = pd.read_excel(xls, "date_terminus")

vel.index = pd.to_datetime(vel["ds"])
vel.index.name = 'date'


def preprocess_data(data, resampling='M'):
    # resample to 'M', 'W', 'D'
    data_resampled = data.reset_index().set_index('date', drop=True).resample(resampling).mean()

    # interpolate to fill in the gaps
    #data_resampled['y'] = data_resampled['y'].interpolate(method='from_derivatives', limit_direction='both', limit=20)
    data_resampled['y'] = data_resampled['y'].interpolate(method='index')

    return data_resampled

# resample data to have uniform temporal frequency
RESAMPLE_FQ = '2W'
data_resampled = preprocess_data(vel, resampling=RESAMPLE_FQ)

# plot and check the resampling against actual data
plt.clf()
plt.plot(vel.index, vel['y'], label='data', lw=2, alpha=0.6)
plt.plot(data_resampled.index, data_resampled['y'], lw=1, label='resampled')
plt.legend(loc='best')
plt.savefig(FIG_DIR + "rink-vel-resampled.png", dpi=300)

class GenerateBatch():
    def __init__(self, data, window_size, xmin, xmax):
        self.window_size = window_size
        self.resolution = (self.xmax - self.xmin)/self.window_size
        self.y_true = data

    def next_batch(self, batch_size, steps):
        # random starting point for each batch
        rand_start = np.random.rand(batch_size,1)
        # convert it to be on time series        
        ts_start = rand_start * (self.xmax - self.xmin - (steps*self.resolution))
        # create batch time series on the x axis
        batch_ts = ts_start + np.arange(0.0, steps+1) * self.resolution

        # create the Y data for the time series x axis from previous step
        y_batch = np.sin(batch_ts)

        if return_batch_ts:
            return y_batch[:,:-1].reshape(-1, steps, 1), y_batch[:, 1:].reshape(-1, steps, 1), batch_ts
        else:
            return y_batch[:,:-1].reshape(-1, steps, 1), y_batch[:, 1:].reshape(-1, steps, 1)



ts_data = GenerateBatch(num_points=10, 

plt.plot(data_resampled['y'])
plt.plot(









